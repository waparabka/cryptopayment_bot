﻿using Telegram.Bot;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace cryptopayment
{
    public class Activity
    {
        TelegramBotClient botClient = new TelegramBotClient("<your-token>");
        Database database = new Database("database.db");
        CryptoPayment payment = new CryptoPayment("<payment-key>", "<merchant-uuid>");

        
        public void Loop()
        {
            botClient.StartReceiving(HandlingUpdates, HandlePollingError, receiverOptions: new ReceiverOptions { AllowedUpdates = { } });

            Console.ReadLine();
        }


        async Task HandlingUpdates(ITelegramBotClient client, Update update, CancellationToken token)
        {
            if (update == null)
                return;
            

            if (update?.Type == UpdateType.Message && update?.Message?.Text != null)
            {
                await HandlingMessages(client, update.Message);

                return;
            }

            if (update?.Type == UpdateType.CallbackQuery)
            {
                await HandlingCallbackQuery(client, update.CallbackQuery);

                return;
            }

            return;
        }
        

        async Task HandlingMessages(ITelegramBotClient client, Message message)
        {
            database.CheckAccountExistment(message.Chat.Id);


            if (message.Text.ToLower().Equals("/start"))
            {
                InlineKeyboardMarkup keyboardMarkup = new(new[]
                {
                    new[]
                    {
                        InlineKeyboardButton.WithCallbackData("Пополнить баланс", "add_funds"),
                    }
                });

                await botClient.SendTextMessageAsync(message.Chat.Id, $"Пользователь: {message.Chat.Id}\nБаланс: {database.GetAccountBalance(message.Chat.Id)}", replyMarkup: keyboardMarkup);

                return;
            }
            
            
            if (message.ReplyToMessage is not null)
            {
                if (message.ReplyToMessage.Text.Contains("Введите сумму"))
                {
                    double amount;
                    
                    if (double.TryParse(message.Text.Replace('.', ','), out amount))
                    {
                        Message lastMessage = await botClient.SendTextMessageAsync(message.Chat.Id, "Генерирую платёж..");
                        
                        dynamic? draft = await payment.CreatePayment(amount, "USD", $"{message.Chat.Id}_{new Random().Next(10000, 99999)}");

                        if (draft is null)
                        {
                            await botClient.EditMessageTextAsync(message.Chat.Id, lastMessage.MessageId, "Ошибка при создании платежа");

                            return;
                        }
                        

                        InlineKeyboardMarkup keyboardMarkup = new(new[]
                        {
                            new[]
                            {
                                InlineKeyboardButton.WithUrl("К оплате", $"{(string)draft.result.url}"),
                                InlineKeyboardButton.WithCallbackData("Проверить платёж", $"check_payment_{(string)draft.result.uuid}")
                            }
                        });
                        
                        database.AddPayment((string)draft.result.uuid, (string)draft.result.order_id, amount, (string)draft.result.status);
                        
                        await botClient.EditMessageTextAsync(message.Chat.Id, lastMessage.MessageId, $"Платёж с уникальным номером\n{(string)draft.result.uuid} успешно создан", replyMarkup: keyboardMarkup);
                    }
                    else
                        await botClient.SendTextMessageAsync(message.Chat.Id, "Некорректная сумма");
                }

                return;
            }

            return;
        }


        async Task HandlingCallbackQuery(ITelegramBotClient client, CallbackQuery callbackQuery)
        {
            if (callbackQuery.Data == null || callbackQuery.Data == string.Empty)
                return;


            if (callbackQuery.Data.Equals("add_funds"))
            {
                await botClient.SendTextMessageAsync(callbackQuery.Message.Chat.Id, "Введите сумму платежа", replyMarkup: new ForceReplyMarkup { Selective = true });
                
                return;
            }


            if (callbackQuery.Data.Contains("check_payment_"))
            {
                Message lastMessage = await botClient.SendTextMessageAsync(callbackQuery.Message.Chat.Id, "Проверяем платёж..");

                dynamic? draft = await payment.GetPaymentInfo(callbackQuery.Data.Replace("check_payment_", ""));
                

                if (draft is null)
                {
                    await botClient.EditMessageTextAsync(callbackQuery.Message.Chat.Id, lastMessage.MessageId, "Ошибка при проверке платежа");

                    return;
                }


                if (database.GetPaymentStatus((string)draft.result.order_id))
                {
                    await botClient.EditMessageTextAsync(callbackQuery.Message.Chat.Id, lastMessage.MessageId, "Платёж уже был оплачен");

                    return;
                }
                

                await botClient.EditMessageTextAsync(callbackQuery.Message.Chat.Id, lastMessage.MessageId, $"Статус платежа: {payment.paymentStates[$"{(string)draft.result.status}"]}");

                if ((string)draft.result.status == "paid" || (string)draft.result.status == "paid_over")
                {
                    database.AddAccountBalance(callbackQuery.Message.Chat.Id, (double)draft.result.amount);
                    
                    database.UpdatePaymentStatus((string)draft.result.status, (string)draft.result.order_id);
                }

                return;
            }
        }



        Task HandlePollingError(ITelegramBotClient client, Exception exception, CancellationToken token) => Task.CompletedTask;
    }
}
