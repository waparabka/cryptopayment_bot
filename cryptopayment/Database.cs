﻿using Microsoft.Data.Sqlite;
using System.Data.Common;

namespace cryptopayment
{
    public class Database
    {
        private SqliteConnection connection;


        public Database(string database)
        {
            connection = new SqliteConnection($"Data Source={database}");
            
            connection.Open();
        }


        public void CheckAccountExistment(long id)
        {
            string sql = $"SELECT COUNT(*) FROM `users` WHERE `id` = '{id}'";

            SqliteCommand cmd = new SqliteCommand(sql, connection);


            if (int.Parse(cmd.ExecuteScalar().ToString()) == 0)
            {
                sql = $"INSERT INTO `users` (`id`, `balance`) VALUES ('{id}', '{0}')";

                new SqliteCommand(sql, connection).ExecuteNonQuery();
            }
        }


        public double GetAccountBalance(long id)
        {
            string sql = $"SELECT `balance` FROM `users` WHERE `id` = '{id}'";

            SqliteCommand cmd = new SqliteCommand(sql, connection);

            object? result = cmd.ExecuteScalar();

            if (result == null)
                return 0;
            else
                return (double)result;
        }


        public void AddAccountBalance(long id, double amount)
        {
            double newBalance = GetAccountBalance(id) + amount;

            string sql = $"UPDATE `users` SET `balance` = '{newBalance.ToString().Replace(',', '.')}' WHERE `id` = '{id}'";

            new SqliteCommand(sql, connection).ExecuteNonQuery();
        }


        public void AddPayment(string uuid, string order_id, double amount, string status)
        {
            string sql = $"INSERT INTO `payments` (`uuid`, `order_id`, `amount`, `status`) VALUES ('{uuid}', '{order_id}', '{amount.ToString().Replace(',', '.')}', '{status}')";

            new SqliteCommand(sql, connection).ExecuteNonQuery();
        }
        

        public bool GetPaymentStatus(string order_id)
        {
            string sql = $"SELECT `status` FROM `payments` WHERE `order_id` LIKE '{order_id}'";

            string? result = new SqliteCommand(sql, connection)?.ExecuteScalar()?.ToString();

            if (result == "paid" || result == "paid_over")
                return true;

            return false;
        }
        

        public void UpdatePaymentStatus(string status, string order_id)
        {
            string sql = $"UPDATE `payments` SET `status` = '{status}' WHERE `order_id` LIKE '{order_id}'";

            new SqliteCommand(sql, connection).ExecuteNonQuery();
        }
    }
}
