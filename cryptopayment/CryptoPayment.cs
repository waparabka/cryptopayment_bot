﻿using Newtonsoft.Json;
using System.Security.Cryptography;
using System.Text;

namespace cryptopayment
{
    public class CryptoPayment
    {
        private string key, merchant;

        private class PaymentInfo
        {
            public string amount { get; set; }
            public string currency { get; set; }
            public string order_id { get; set; }
        }


        private class PaymentStatus
        {
            public string uuid { get; set; }
        }



        public CryptoPayment(string key, string merchant)
        {
            this.key = key;
            this.merchant = merchant;
        }
        

        public Dictionary<string, string> paymentStates = new Dictionary<string, string>
        {
            { "paid", "Платеж прошел успешно" },
            { "paid_over", "Платеж прошел успешно и плательщик заплатил больше" },
            { "wrong_amount", "Клиент заплатил меньше, чем требовалось" },
            { "process", "Оплата в обработке" },
            { "confirm_check", "Оплата в блокчейне" },
            { "wrong_amount_waiting", "Клиент заплатил меньше, чем требовалось, с возможностью доплаты" },
            { "check", "Платеж проверен и ожидает оплаты" },
            { "fail", "Ошибка оплаты" },
            { "cancel", "Платеж отменен" }
        };


        private string GeneratePaymentSignature(string json)
        {
            byte[] jsonBytes = Encoding.UTF8.GetBytes(json);

            MD5 hashEngine = MD5.Create();

            byte[] inputBytes = Encoding.ASCII.GetBytes(Convert.ToBase64String(jsonBytes) + key);
            byte[] hashBytes = hashEngine.ComputeHash(inputBytes);

            return Convert.ToHexString(hashBytes).ToLower();
        }


        public async Task<dynamic?> CreatePayment(double amount, string currency, string order_id)
        {
            HttpClient client = new HttpClient();

            string url = "https://api.cryptomus.com/v1/payment";


            PaymentInfo paymentInfo = new PaymentInfo();
            
            paymentInfo.amount = amount.ToString().Replace(',', '.');
            paymentInfo.currency = currency;
            paymentInfo.order_id = order_id;

            string body = JsonConvert.SerializeObject(paymentInfo);
            string sign = GeneratePaymentSignature(body);


            Dictionary<string, string> headers = new Dictionary<string, string>
            {
                { "merchant", merchant },
                { "sign", sign }
            };

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url) { Content = new StringContent(body, Encoding.UTF8, "application/json") };
            

            foreach (var header in headers)
                request.Headers.Add(header.Key, header.Value);

            
            HttpResponseMessage response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode)
                return null;
            
            
            string content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject(content);
        }


        public async Task<dynamic?> GetPaymentInfo(string uuid)
        {
            HttpClient client = new HttpClient();
            
            string url = "https://api.cryptomus.com/v1/payment/info";


            PaymentStatus paymentStatus = new PaymentStatus();

            paymentStatus.uuid = uuid;

            string body = JsonConvert.SerializeObject(paymentStatus);
            string sign = GeneratePaymentSignature(body);
            

            Dictionary<string, string> headers = new Dictionary<string, string>
            {
                { "merchant", merchant },
                { "sign", sign }
            };

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url) { Content = new StringContent(body, Encoding.UTF8, "application/json") };


            foreach (var header in headers)
                request.Headers.Add(header.Key, header.Value);


            HttpResponseMessage response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode)
                return null;
            

            string content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject(content);
        }
    }
}
