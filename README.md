# Cryptopayment BOT

Good example of using a crypto payment system in your telegram bot

## Getting started

Clone project from this repository

```
git clone https://gitlab.com/waparabka/cryptopayment_bot.git
```

Replace telegram bot token on yours and [cryptomus](https://cryptomus.com) payment key + merchant uuid

```
TelegramBotClient botClient = new TelegramBotClient("<your-token>");
CryptoPayment payment = new CryptoPayment("<payment-key>", "<merchant-uuid>");
```

# Dependencies

[Telegram.Bot](https://www.nuget.org/packages/Telegram.Bot)
[Microsoft.Data.Sqlite](https://www.nuget.org/packages/Microsoft.Data.Sqlite)